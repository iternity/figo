module gitlab.com/iternity/figo

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/alecthomas/gometalinter v3.0.0+incompatible
	github.com/alecthomas/units v0.0.0-20151022065526-2efee857e7cf // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/shlex v0.0.0-20181106134648-c34317bd91bf // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/matryer/moq v0.0.0-20190312154309-6cfb0558e1bd
	github.com/nicksnyder/go-i18n v1.10.0 // indirect
	github.com/pelletier/go-toml v1.2.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.3.0
	golang.org/x/tools v0.0.0-20190312151545-0bb0c0a6e846
	gopkg.in/alecthomas/kingpin.v3-unstable v3.0.0-20171010053543-63abe20a23e2 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)

// gometalinter currently only supports 63abe20a23e2 (which is vendored in
// gometalinter's repository - as are all dependencies). Newer kingpin commits
// have to be explicitly exluded!
exclude gopkg.in/alecthomas/kingpin.v3-unstable v3.0.0-20180810215634-df19058c872c
