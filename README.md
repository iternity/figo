# figo

figo is a Go wrapper for [fio](https://github.com/axboe/fio) the _Flexible I/O
Tester_.

The purpose of this project is to be able to execute _fio_ tests and parse the
result files (containing performance data). _figo_ is meant to be used as a
library to build tools and applications.


## Installation

```bash
go get -u gitlab.com/iternity/figo
```


## Usage

```go
package main

import (
	"fmt"
	"io/ioutil"

	"gitlab.com/iternity/figo/io/json"
)

func main() {
    content, err := ioutil.ReadFile("fio-result.json")
    if err != nil {
        panic(err)
    }

    report, err := json.Parse(string(content))
    if err != nil {
        panic(err)
    }

    fmt.Println(report.FioVersion())
}
```


## Development

**Note:** We assume you are using Go 1.11 to take advantage of [go
modules](https://github.com/golang/go/wiki/Modules).

The dependency Gometalinter doesn't work properly with go modules:

* [How to use gometaliner for projects with go modules and no vendor?
  #562](https://github.com/alecthomas/gometalinter/issues/562)

Therefore to execute `make lint` you need to check out the repository under the
correct $GOPATH path. In all other cases you may use any path.

```bash
git clone https://gitlab.com/iternity/figo
cd figo
make test
```

**core** contains the data structures and business logic that should remain
stable across _fio_ versions and be exposed as a public interface.

**io** contains interactions with any external component (such as parsing JSON
files).

Currently the API of this package is not stable and may change in a
non-backward compatible way.

_fio_ JSON file parsing is currently **incomplete**. Also _fio.Report{}_ and
_fio.Job{}_ are still incomplete.

Pull requests are welcome. If you encounter issues or have suggestions please
file a ticket on [GitLab](https://gitlab.com/iternity/figo/issues/).


## License

The _figo_ project is licensed under the [MIT License](LICENSE).
