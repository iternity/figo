// +build devtools

// devtools is a package whose only purpose is to import all tools used during
// development so that they can be versioned with go modules. It is never
// compiled, executed or used anywhere.
package devtools

import (
	_ "github.com/alecthomas/gometalinter"
	_ "github.com/matryer/moq"
	_ "golang.org/x/tools/cmd/goimports"
)
