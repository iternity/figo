SHELL=/bin/bash

export PATH := $(PWD)/bin:$(PATH)
export GOBIN := $(PWD)/bin

# Fully qualified repository / package name
FQ_NAME = gitlab.com/iternity/figo

# Go files excluding vendor
GO_FILES = $(shell find . -name "*.go" -not -path "./vendor/*" -type f)

all:
	@echo "fmt        Format all Go files with goimports"
	@echo "lint       Execute gometalinter"
	@echo "test       Execute unit tests"
	@echo "tools      Install dev tools such as moq and gometalinter to ./bin"
	@echo "update     Update all dependencies"

.PHONY: fmt
fmt: tools
	@GO111MODULE=on goimports -l -w -local $(FQ_NAME) $(GO_FILES)

.PHONY: test
test:
	@go test ./...

.PHONY: lint
lint: tools
	GO111MODULE=off gometalinter --exclude=vendor ./...

.PHONY: tools
tools:
	GO111MODULE=on go install github.com/matryer/moq
	GO111MODULE=on go install github.com/alecthomas/gometalinter
	GO111MODULE=on go install golang.org/x/tools/cmd/goimports
	GO111MODULE=on go mod tidy
	GO111MODULE=on go mod vendor
	GO111MODULE=off gometalinter --install

.PHONY: update
update:
	GO111MODULE=on go get -u
	GO111MODULE=on go mod tidy
	GO111MODULE=on go mod vendor
	make tools
