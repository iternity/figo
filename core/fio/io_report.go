package fio

import "time"

type IOReport struct {
	ioBytes  int64
	runtime  time.Duration
	totalIOs int64
}

func (I *IOReport) TotalIOs() int64 {
	return I.totalIOs
}

func (I *IOReport) SetTotalIOs(ios int64) {
	I.totalIOs = ios
}

func (I *IOReport) Runtime() time.Duration {
	return I.runtime
}

func (I *IOReport) SetRuntime(runtime time.Duration) {
	I.runtime = runtime
}

func (I *IOReport) IOBytes() int64 {
	return I.ioBytes
}

func (I *IOReport) SetIOBytes(ioBytes int64) {
	I.ioBytes = ioBytes
}

// Bandwidth returns the value in bytes / sec
func (I *IOReport) Bandwidth() float64 {
	runtime := I.runtimeInSecs()
	if runtime == 0.0 {
		return 0.0
	}
	return float64(I.ioBytes) / runtime
}

// IOPS returns the IO operations per second
func (I *IOReport) IOPS() float64 {
	runtime := I.runtimeInSecs()
	if runtime == 0.0 {
		return 0.0
	}
	return float64(I.totalIOs) / runtime
}

// runtimeInSecs returns the IOReport runtime in seconds
func (I *IOReport) runtimeInSecs() float64 {
	return float64(I.runtime) / float64(time.Second)
}
