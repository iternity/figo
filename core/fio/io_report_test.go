package fio_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/iternity/figo/core/fio"
)

// Verify that the bandwith is calculated correctly when runtime is 0
func TestIOReport_Bandwidth_ZeroRuntime(t *testing.T) {
	report := fio.IOReport{}
	report.SetIOBytes(0)
	report.SetRuntime(0 * time.Second)
	assert.Equal(t, 0.0, report.Bandwidth())
}

// Verify that the IOPS are calculated correctly when runtime is 0
func TestIOReport_IOPS_ZeroRuntime(t *testing.T) {
	report := fio.IOReport{}
	report.SetTotalIOs(0)
	report.SetRuntime(0 * time.Second)
	assert.Equal(t, 0.0, report.IOPS())
}
