package fio

import "time"

type Job struct {
	name    string
	options map[string]string
	groupID int
	elapsed time.Duration
	eta     time.Duration
	error   int
	read    *IOReport
	write   *IOReport
	trim    *IOReport
}

func (j *Job) Trim() *IOReport {
	return j.trim
}

func (j *Job) SetTrim(trim *IOReport) {
	j.trim = trim
}

func (j *Job) Write() *IOReport {
	return j.write
}

func (j *Job) SetWrite(write *IOReport) {
	j.write = write
}

func (j *Job) Read() *IOReport {
	return j.read
}

func (j *Job) SetRead(read *IOReport) {
	j.read = read
}

func (j *Job) Error() int {
	return j.error
}

func (j *Job) SetError(error int) {
	j.error = error
}

func (j *Job) ETA() time.Duration {
	return j.eta
}

func (j *Job) SetETA(eta time.Duration) {
	j.eta = eta
}

func (j *Job) Elapsed() time.Duration {
	return j.elapsed
}

func (j *Job) SetElapsed(elapsed time.Duration) {
	j.elapsed = elapsed
}

func (j *Job) GroupID() int {
	return j.groupID
}

func (j *Job) SetGroupID(groupID int) {
	j.groupID = groupID
}

func (j *Job) Options() map[string]string {
	return j.options
}

func (j *Job) SetOptions(options map[string]string) {
	j.options = options
}

func (j *Job) Name() string {
	return j.name
}

func (j *Job) SetName(name string) {
	j.name = name
}
