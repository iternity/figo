package fio

import "time"

type Report struct {
	fioVersion    string
	time          time.Time
	globalOptions map[string]string
	jobs          []Job
}

func (r *Report) Jobs() []Job {
	return r.jobs
}

func (r *Report) SetJobs(jobs []Job) {
	r.jobs = jobs
}

func (r *Report) GlobalOptions() map[string]string {
	return r.globalOptions
}

func (r *Report) SetGlobalOptions(globalOptions map[string]string) {
	r.globalOptions = globalOptions
}

func (r *Report) FioVersion() string {
	return r.fioVersion
}

func (r *Report) SetFioVersion(v string) {
	r.fioVersion = v
}

func (r *Report) Time() time.Time {
	return r.time
}

func (r *Report) SetTime(t time.Time) {
	r.time = t
}
