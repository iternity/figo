package json_test

import (
	"io/ioutil"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/iternity/figo/io/json"
)

func TestParse(t *testing.T) {
	path := filepath.Join(
		"testdata",
		"1MiB_x_400_4T-20181216-133232-result.json",
	)
	content, err := ioutil.ReadFile(path)

	require.Nil(t, err)

	report, err := json.Parse(string(content))

	require.Nil(t, err)
	require.NotNil(t, report)

	assert.Equal(t, "fio-3.12", report.FioVersion())

	expectedTime := time.Date(
		2018,
		12,
		16,
		13,
		33,
		2,
		498*1000*1000, // 498 Milliseconds
		time.UTC,
	)

	assert.Equal(t, expectedTime, report.Time())

	expectedOptions := map[string]string{
		"create_on_open": "1",
		"kb_base":        "1000",
	}

	assert.Equal(t, expectedOptions, report.GlobalOptions())

	jobs := report.Jobs()

	require.Len(t, jobs, 1)

	job0 := jobs[0]

	assert.Equal(t, "1MiB_x_400_4T", job0.Name())
	assert.Equal(t, 0, job0.GroupID())

	expectedJobOptions := map[string]string{
		"name":              "1MiB_x_400_4T",
		"filesize":          "1MiB",
		"openfiles":         "1",
		"file_service_type": "sequential",
		"thread":            "4",
		"directory":         "fio/1MiB_x_400_4T",
		"rw":                "write",
		"nrfiles":           "100",
		"numjobs":           "4",
		"bs":                "512K",
	}

	assert.Equal(t, expectedJobOptions, job0.Options())

	assert.Equal(t, 29*time.Second, job0.Elapsed())
	assert.Equal(t, 0*time.Second, job0.ETA())

	assert.Equal(t, 0, job0.Error())

	assert.Equal(t, 28513*time.Millisecond, job0.Write().Runtime())
	assert.Equal(t, int64(800), job0.Write().TotalIOs())
	assert.Equal(t, int64(409600000), job0.Write().IOBytes())
	assert.InDelta(t, 14365377.0, job0.Write().Bandwidth(), 0.2)
	assert.InDelta(t, 28.057377, job0.Write().IOPS(), 0.000002)

	assert.Equal(t, time.Duration(0), job0.Read().Runtime())
	assert.Equal(t, int64(0), job0.Read().TotalIOs())
	assert.Equal(t, int64(0), job0.Read().IOBytes())
	assert.Equal(t, 0.0, job0.Read().Bandwidth())
	assert.Equal(t, 0.0, job0.Read().IOPS())

	assert.Equal(t, time.Duration(0), job0.Trim().Runtime())
	assert.Equal(t, int64(0), job0.Trim().TotalIOs())
	assert.Equal(t, int64(0), job0.Trim().IOBytes())
	assert.Equal(t, 0.0, job0.Trim().Bandwidth())
	assert.Equal(t, 0.0, job0.Trim().IOPS())
}

func TestParse_Error(t *testing.T) {
	report, err := json.Parse("")

	assert.Error(t, err)
	assert.Nil(t, report)
	assert.Contains(t, err.Error(), "unexpected end")
}
