package json

import (
	"encoding/json"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/iternity/figo/core/fio"
)

func Parse(content string) (*fio.Report, error) {
	jsonReport := Report{}
	report := fio.Report{}
	err := json.Unmarshal([]byte(content), &jsonReport)
	if err != nil {
		err = errors.Wrap(err, "could not unmarshal JSON report")
		return nil, err
	}

	report.SetFioVersion(jsonReport.FioVersion)

	// Sun Dec 16 13:33:02.498 2018
	sec := jsonReport.TimestampMs / 1000
	ms := jsonReport.TimestampMs % 1000
	tm := time.Unix(sec, ms*1000*1000)
	report.SetTime(tm.UTC())

	report.SetGlobalOptions(jsonReport.GlobalOptions)

	var jobs []fio.Job
	for _, jsonJob := range jsonReport.Jobs {
		j := fio.Job{}
		j.SetName(jsonJob.JobName)
		j.SetGroupID(jsonJob.GroupID)
		j.SetOptions(jsonJob.JobOptions)
		j.SetElapsed(time.Second * time.Duration(jsonJob.Elapsed))
		j.SetETA(time.Second * time.Duration(jsonJob.ETA))
		j.SetError(jsonJob.Error)

		readReport := &fio.IOReport{}
		readReport.SetIOBytes(jsonJob.Read.IOBytes)
		readReport.SetRuntime(time.Millisecond * time.Duration(jsonJob.Read.Runtime))
		readReport.SetTotalIOs(jsonJob.Read.TotalIOs)

		writeReport := &fio.IOReport{}
		writeReport.SetIOBytes(jsonJob.Write.IOBytes)
		writeReport.SetRuntime(time.Millisecond * time.Duration(jsonJob.Write.Runtime))
		writeReport.SetTotalIOs(jsonJob.Write.TotalIOs)

		trimReport := &fio.IOReport{}
		trimReport.SetIOBytes(jsonJob.Trim.IOBytes)
		trimReport.SetRuntime(time.Millisecond * time.Duration(jsonJob.Trim.Runtime))
		trimReport.SetTotalIOs(jsonJob.Trim.TotalIOs)

		j.SetRead(readReport)
		j.SetWrite(writeReport)
		j.SetTrim(trimReport)

		jobs = append(jobs, j)
	}

	report.SetJobs(jobs)

	return &report, err
}
