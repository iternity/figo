package json

type Job struct {
	JobName    string            `json:"jobname"`
	GroupID    int               `json:"groupid"`
	JobOptions map[string]string `json:"job options"`
	Elapsed    int64             `json:"elapsed"`
	ETA        int64             `json:"eta"`
	Error      int               `json:"error"`
	Read       IOReport          `json:"read"`
	Write      IOReport          `json:"write"`
	Trim       IOReport          `json:"trim"`
}
