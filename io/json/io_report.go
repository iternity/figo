package json

type IOReport struct {
	IOBytes  int64   `json:"io_bytes"`
	IOkBytes int64   `json:"io_kbytes"`
	BWBytes  int64   `json:"bw_bytes"`
	BW       int64   `json:"bw"`
	IOPS     float64 `json:"iops"`
	Runtime  int64   `json:"runtime"`
	TotalIOs int64   `json:"total_ios"`
}
