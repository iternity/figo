package json

type Report struct {
	FioVersion    string            `json:"fio version"`
	Timestamp     int64             `json:"timestamp"`
	TimestampMs   int64             `json:"timestamp_ms"`
	Time          string            `json:"time"`
	GlobalOptions map[string]string `json:"global options"`
	Jobs          []Job             `json:"jobs"`
}
